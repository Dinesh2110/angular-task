import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  thirdbody: any;
  constructor(private routes: ActivatedRoute) {
    this.routes.queryParams.subscribe((get) => {
      this.thirdbody = JSON.parse(get['data']);
    });
  }
  // thirdbody = [
  //   {
  //     insurancecompany: 'Baharin National Insurance Co',

  //     file: 54,

  //     tottime: 777,
  //   },

  //   {
  //     insurancecompany: 'NIA- International Agencies Co',

  //     file: 0,

  //     tottime: 2,
  //   },

  //   {
  //     insurancecompany: 'SNIC',

  //     file: 0,

  //     tottime: 3,
  //   },

  //   {
  //     insurancecompany: 'Gulf Union Insurance& Reinsurance Claims ',

  //     file: 0,

  //     tottime: 3,
  //   },

  //   {
  //     insurancecompany: 'GIG Baharin Takaful',

  //     file: 0,

  //     tottime: 4,
  //   },

  //   {
  //     insurancecompany: 'AXA  Insurance ',

  //     file: 0,

  //     tottime: 2,
  //   },

  //   {
  //     insurancecompany: 'No Insurance Company',

  //     file: 0,

  //     tottime: 5,
  //   },
  // ];

  secondtable = [
    {
      claims: 'MOT-CL-004256',

      reported: '',

      status: 'APPROVED',
    },

    {
      claims: 'MOT-CL-004123',

      reported: '20/12/2022',

      status: 'PAID',
    },

    {
      claims: 'MOT-CL-004227',

      reported: '',

      status: 'PAID',
    },

    {
      claims: 'MOT-CL-009808',

      reported: '',

      status: 'PAID',
    },

    {
      claims: 'MOT-CL-004244',

      reported: '6/12/2022',

      status: 'PAID',
    },

    {
      claims: 'MOT-CL-004256',

      reported: '',

      status: 'APPROVED',
    },

    {
      claims: 'MOT-CL-004256',

      reported: '',

      status: 'APPROVED',
    },
  ];
}
