import { JsonPipe } from '@angular/common';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-receivable',
  templateUrl: './receivable.component.html',
  styleUrls: ['./receivable.component.css'],
})
export class ReceivableComponent {
  // @Output() public sendData = new EventEmitter<String>();
  reactiveForms: FormGroup;
  constructor(private routing: Router, private formBuilder: FormBuilder) {
    this.reactiveForms = new FormGroup({
      company: new FormControl(),
      file: new FormControl(),
      time: new FormControl(),
    });
  }
  ngOnInit() {}

  onSubmit() {
    let data = this.reactiveForms.value;

    this.routing.navigate(['/dash'], {
      queryParams: { data: JSON.stringify(data) },
    });
  }
}
