import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashfinalComponent } from './dashfinal.component';

describe('DashfinalComponent', () => {
  let component: DashfinalComponent;
  let fixture: ComponentFixture<DashfinalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashfinalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashfinalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
