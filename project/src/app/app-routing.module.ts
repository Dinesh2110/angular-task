import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashfinalComponent } from './dashfinal/dashfinal.component';
import { ReceivableComponent } from './receivable/receivable.component';

const routes: Routes = [
  { path: 'dash', component: DashfinalComponent },
  { path: 'form', component: ReceivableComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
