import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ExternalService {
  tableClaims: number = 20;
  tableUnClaims: number = 55;
}
