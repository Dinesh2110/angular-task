import { Component, OnInit } from '@angular/core';
import { Chart, registerables } from 'chart.js';
import { ExternalService } from '../service';
Chart.register(...registerables);

export interface PeriodicElement {
  name: string;

  weight: number;
  symbol: number;
  vijay: number;
  ajith: number;
  surya: number;
  rajini: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {
    name: 'Bahrain National Insurance Co. B.S.C.',
    weight: 1.0079,
    symbol: 4,
    vijay: 4,
    ajith: 4,
    surya: 4,
    rajini: 4,
  },
  {
    name: 'No Insurance Company',
    weight: 4.0026,
    symbol: 4,
    vijay: 4,
    ajith: 4,
    surya: 4,
    rajini: 4,
  },
];

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
})
export class ChartComponent implements OnInit {
  markblue: number;
  markyellow: number;
  constructor(private service: ExternalService) {
    this.markblue = this.service.tableClaims;
    this.markyellow = this.service.tableUnClaims;
  }
  displayedColumns: string[] = [
    'name',
    'weight',
    'symbol',
    'vijay',
    'ajith',
    'surya',
    'rajini',
  ];
  dataSource = ELEMENT_DATA;

  barChartOptions = {};
  ngOnInit(): void {
    this.renderChart();
  }
  renderChart() {
    var myChart = new Chart('piechart', {
      type: 'bar',

      data: {
        labels: ['', '', '', 'BMI', '', ''],

        datasets: [
          {
            label: 'Approved',

            data: [0, 0, 0, `${this.markblue}`, 0, 0],

            backgroundColor: '#0196FD',

            borderColor: '#0196FD',

            borderWidth: 1,
          },

          {
            label: 'Total Claims',

            data: [0, 0, 0, `${this.markyellow}`, 0, 0],

            backgroundColor: '#FFAF00',

            borderColor: '#FFAF00',

            borderWidth: 1,
          },
        ],
      },

      options: {
        animation: {
          duration: 0,
        },
        scales: {
          x: { display: false },
          y: {
            beginAtZero: true,
          },
        },
      },
    });
  }
}
