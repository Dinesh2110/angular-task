import { Component } from '@angular/core';

interface Language {
  value: string;
  viewValue: string;
}

interface Admin {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css'],
})
export class ToolbarComponent {
  opened = true;
  language: Language[] = [
    { value: 'Tamil', viewValue: 'Tamil' },
    { value: 'English', viewValue: 'English' },
    { value: 'Hindi', viewValue: 'Hindi' },
  ];

  admin: Admin[] = [
    { value: 'Manager', viewValue: 'Manager' },
    { value: 'Team leader', viewValue: 'Team Leader' },
    { value: 'Developer', viewValue: 'Developer' },
  ];
}
