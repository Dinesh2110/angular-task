import { Component } from '@angular/core';
import { ExternalService } from '../service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent {
  rise: number;
  low: number;
  constructor(private service: ExternalService) {
    this.rise = this.service.tableClaims;
    this.low = this.service.tableUnClaims;
  }
}
